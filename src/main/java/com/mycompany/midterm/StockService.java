/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class StockService implements Serializable {
    private static ArrayList<Stock> stockList = new ArrayList<>();
    
    static {
        
    }

    public static boolean addStock(Stock stock) {
        stockList.add(stock);
        save();
        return true;
    }

    public static boolean delStock(Stock stock) {
        stockList.remove(stock);
        save();
        return true;
    }

    public static boolean delStock(int index) {
        stockList.remove(index);
        save();
        return true;
    }
    
    
      public static boolean delStockItem() {
        stockList.clear();
        save();
        return true;
    }

    public static ArrayList<Stock> getStock() {
        return stockList;
    }
    
    
    public static Stock getStock(int index){
        return stockList.get(index);
    }
    

    public static boolean updateStock(int index, Stock stock) {
        stockList.set(index, stock);
        save();
        return true;
    }

    public static void save() {
         File file = null;
         FileOutputStream fos = null;
         ObjectOutputStream oos = null;
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(stockList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
         File file = null;
         FileInputStream fis = null;
         ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            stockList = (ArrayList<Stock>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StockService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        public static double getTotal() {
         double sum = 0;
         for(Stock s :stockList){
             sum = sum+(s.getPrice()*s.getAmount());
         }
         return sum;
        }
         
         public static int getItem(){
         int item = 0;
         for(Stock s :stockList){
             item = item+s.getAmount();
         }
         return item;
    }

}
