/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class Stock implements Serializable {
    
    private String no;
    private String name;
    private String brand;
    double price;
    private int amount;
    
    public Stock(String no,String name,String brand,double price,int amount) {
        this.no = no;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    

    @Override
    public String toString() {
       return "ID : "+no+"   Name : "+ name +
       "   Brand : "+ brand +"   Price : "+ price +"   Amount : "+ amount;
    }
    
   
    
    
}
